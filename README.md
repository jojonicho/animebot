**[Jonathan Nicholas](https://gitlab.com/jojonicho)**
1906293133

[![pipeline status](https://gitlab.com/jojonicho/animebot/badges/joni/anime/pipeline.svg)](https://gitlab.com/jojonicho/animebot/-/commits/joni/anime) [![coverage report](https://gitlab.com/jojonicho/animebot/badges/joni/anime/coverage.svg)](https://gitlab.com/jojonicho/animebot/-/commits/joni/anime)

**[Handi](https://gitlab.com/handi91)**
1906293064

[![pipeline status](https://gitlab.com/jojonicho/animebot/badges/handi/friend/pipeline.svg)](https://gitlab.com/jojonicho/animebot/-/commits/handi/friend) [![coverage report](https://gitlab.com/jojonicho/animebot/badges/handi/friend/coverage.svg)](https://gitlab.com/jojonicho/animebot/-/commits/handi/friend)

**[Fahdii Ajmalal Fikrie](https://gitlab.com/fahdikrie)**
1906293064

[![pipeline status](https://gitlab.com/jojonicho/animebot/badges/fahdii/manga/pipeline.svg)](https://gitlab.com/jojonicho/animebot/-/commits/fahdii/manga) [![coverage report](https://gitlab.com/jojonicho/animebot/badges/fahdii/manga/coverage.svg)](https://gitlab.com/jojonicho/animebot/-/commits/fahdii/manga)

**[M Raihan A](https://gitlab.com/raihan.armon)**
1806141372

[![pipeline status](https://gitlab.com/jojonicho/animebot/badges/raihan/minigames/pipeline.svg)](https://gitlab.com/jojonicho/animebot/-/commits/raihan/minigames) [![coverage report](https://gitlab.com/jojonicho/animebot/badges/raihan/minigames/coverage.svg)](https://gitlab.com/jojonicho/animebot/-/commits/raihan/minigames)

**[Muhammad Farhan Ghaffar](https://gitlab.com/muhammad.farhan84)**
1806186742

[![pipeline status](http://gitlab.com/jojonicho/animebot/badges/farhan/schedule-and-notifier/pipeline.svg)](https://gitlab.com/jojonicho/animebot/-/commits/farhan/schedule-and-notifier) [![coverage report](http://gitlab.com/jojonicho/animebot/badges/farhan/schedule-and-notifier/coverage.svg)](https://gitlab.com/jojonicho/animebot/-/commits/farhan/schedule-and-notifier)

# AnimeBot

A bot showcasing the Command Client in the JDA-Utilities library, extended with Jikan4Java, Postgres, and Gradle.

# Dependencies

* [JDA](https://github.com/DV8FromTheWorld/JDA)
* [JDA-Utilities](https://github.com/JDA-Applications/JDA-Utilities)
* Jikan4Java
* Postgres

# Prerequisites

1. replace `src/main/resources/application.yml.com.jojonicho.animebot.example` with `src/main/resources/application.yml`
   and don't forget to add your own bot's token
2. make sure postgres is running, customize it in `src/main/resources/application.properties`

# Resources

1. [Setup Bot, get token, and invite to server](https://www.youtube.com/watch?v=jGrD8AZfTig&ab_channel=techtoolbox)
2. [Read about JDA](https://www.youtube.com/watch?v=jGrD8AZfTig&ab_channel=techtoolbox)
3. [Read about Jikan4Java](https://www.youtube.com/watch?v=jGrD8AZfTig&ab_channel=techtoolbox)
4. [Jikan API Docs](https://jikan.docs.apiary.io/)
