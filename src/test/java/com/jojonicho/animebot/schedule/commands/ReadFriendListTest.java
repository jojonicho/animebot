package com.jojonicho.animebot.schedule.commands;

import com.jagrosh.jdautilities.command.CommandEvent;
import com.jojonicho.animebot.anime.model.AnimeEntry;
import com.jojonicho.animebot.anime.service.AnimeEntryService;
import com.jojonicho.animebot.discorduser.model.DiscordUser;
import com.jojonicho.animebot.discorduser.service.DiscordUserServiceImpl;
import com.jojonicho.animebot.friend.model.FriendRequest;
import com.jojonicho.animebot.friend.service.FriendRequestService;
import net.dv8tion.jda.api.entities.User;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class ReadFriendListTest {
    @InjectMocks
    ReadFriendsList readFriendsList;

    @Mock
    CommandEvent event;

    @Mock
    private DiscordUserServiceImpl discordUserService;

    @Mock
    private FriendRequestService friendRequestService;

    @Mock
    private AnimeEntryService animeEntryService;

    @Mock
    private User user;

    private DiscordUser discordUser;
    private DiscordUser friendUser;
    private FriendRequest mockFriendRequest;
    private final String DISCORD_ID = "264258056416657419";
    private final String FRIEND_ID = "708209585407655966";
    private final String ARGS = "<@!708209585407655966>";
    private AnimeEntry mockAnimeEntry;

    // demon slayer
    private final String ANIME_ID = "42249";
    private final String COMPLETED = "COMPLETED";
    private final String SCORE = "10";

    @BeforeEach
    public void setUp() {
        discordUser = new DiscordUser();
        discordUser.setId(DISCORD_ID);

        friendUser = new DiscordUser();
        friendUser.setId(FRIEND_ID);

        mockFriendRequest = new FriendRequest();
        mockFriendRequest.setDiscordUser(discordUser);
        mockFriendRequest.setFriendUser(friendUser);
        mockFriendRequest.setUserName("dummy");
        mockFriendRequest.setFriendName("dummy2");
        mockFriendRequest.setState("ACCEPTED");

        mockAnimeEntry = new AnimeEntry();
        mockAnimeEntry.setDiscordUser(discordUser);
        mockAnimeEntry.setMalId(Integer.parseInt(ANIME_ID));
    }

    @Test
    public void testExecute() {
        Map<String, DiscordUser> friendList = new HashMap<String, DiscordUser>();
        friendList.put("dummy2", friendUser);

        List<AnimeEntry> list = new ArrayList<>();
        list.add(mockAnimeEntry);

        when(event.getAuthor())
                .thenReturn(user);
        when(user.getId())
                .thenReturn(DISCORD_ID);
        when(event.getArgs())
                .thenReturn(ARGS);
        when(discordUserService.getDiscordUser(DISCORD_ID))
                .thenReturn(discordUser);
        when(friendRequestService.getAllFriend(discordUser))
                .thenReturn(friendList);
        when(animeEntryService.getListAnimeEntry(FRIEND_ID))
                .thenReturn(list);
        readFriendsList.execute(event);
    }

    @Test
    public void testExecuteEmptyList() {
        Map<String, DiscordUser> friendList = new HashMap<String, DiscordUser>();
        friendList.put("dummy2", friendUser);

        List<AnimeEntry> list = new ArrayList<>();

        when(event.getAuthor())
                .thenReturn(user);
        when(user.getId())
                .thenReturn(DISCORD_ID);
        when(event.getArgs())
                .thenReturn(ARGS);
        when(discordUserService.getDiscordUser(DISCORD_ID))
                .thenReturn(discordUser);
        when(friendRequestService.getAllFriend(discordUser))
                .thenReturn(friendList);
        when(animeEntryService.getListAnimeEntry(FRIEND_ID))
                .thenReturn(list);
        readFriendsList.execute(event);
    }
}
