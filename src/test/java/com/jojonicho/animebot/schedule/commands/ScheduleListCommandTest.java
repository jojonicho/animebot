package com.jojonicho.animebot.schedule.commands;

import com.jagrosh.jdautilities.command.CommandEvent;
import com.jojonicho.animebot.anime.model.AnimeEntry;
import com.jojonicho.animebot.anime.service.AnimeEntryService;
import com.jojonicho.animebot.discorduser.model.DiscordUser;
import com.jojonicho.animebot.schedule.commands.ScheduleListCommand;
import net.dv8tion.jda.api.entities.User;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.junit.jupiter.api.extension.ExtendWith;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class ScheduleListCommandTest {

    @InjectMocks
    ScheduleListCommand scheduleListCommand;

    @Mock
    CommandEvent event;

    // demon slayer
    private final String ANIME_ID = "42249";

    private final String COMPLETED = "COMPLETED";
    private final String SCORE = "10";

    private final String DISCORD_ID = "264258056416657419";

    @Mock
    private AnimeEntryService animeEntryService;

    private DiscordUser discordUser;

    private AnimeEntry mockAnimeEntry;

    @Mock
    private User user;

    @BeforeEach
    public void setUp(){
        discordUser = new DiscordUser();
        discordUser.setId(DISCORD_ID);

        mockAnimeEntry = new AnimeEntry();
        mockAnimeEntry.setDiscordUser(discordUser);
        mockAnimeEntry.setMalId(Integer.parseInt(ANIME_ID));
    }

    @Test
    public void testExecute() {
        List<AnimeEntry> list = new ArrayList<>();
        list.add(mockAnimeEntry);

        when(event.getAuthor())
                .thenReturn(user);
        when(user.getId())
                .thenReturn(DISCORD_ID);
        when(animeEntryService.getListAnimeEntry(DISCORD_ID))
                .thenReturn(list);
        scheduleListCommand.execute(event);
    }

    @Test
    public void testExecuteEmptyList() {
        List<AnimeEntry> list = new ArrayList<>();

        when(event.getAuthor())
                .thenReturn(user);
        when(user.getId())
                .thenReturn(DISCORD_ID);
        when(animeEntryService.getListAnimeEntry(DISCORD_ID))
                .thenReturn(list);
        scheduleListCommand.execute(event);
    }
}