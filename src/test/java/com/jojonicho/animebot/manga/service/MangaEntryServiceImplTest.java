package com.jojonicho.animebot.manga.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

import com.jojonicho.animebot.discorduser.model.DiscordUser;
import com.jojonicho.animebot.manga.model.MangaEntry;
import com.jojonicho.animebot.manga.repository.MangaEntryRepository;
import java.util.ArrayList;
import java.util.List;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
public class MangaEntryServiceImplTest {

    @Mock
    private MangaEntryRepository mangaEntryRepository;

    @InjectMocks
    private MangaEntryServiceImpl mangaEntryService;

    private DiscordUser discordUser;

    private MangaEntry mockMangaEntry;

    @BeforeEach
    public void setUp() {
        discordUser = new DiscordUser();
        discordUser.setId("1");

        mockMangaEntry = new MangaEntry();
        mockMangaEntry.setDiscordUser(discordUser);
        mockMangaEntry.setMalId(1);
    }

    @Test
    public void testServiceCreate() {
        when(mangaEntryRepository.save(any()))
            .thenReturn(mockMangaEntry);
        MangaEntry mangaEntry = mangaEntryService.createMangaEntry(1, discordUser);
        assertEquals(mangaEntry, mockMangaEntry);
    }

    @Test
    public void testServiceCreateDuplicateManga() {
        when(mangaEntryRepository.findByMalIdAndDiscordUser(mockMangaEntry.getMalId(), discordUser))
            .thenReturn(mockMangaEntry);

        MangaEntry mangaEntry = mangaEntryService.createMangaEntry(1, discordUser);
        assertEquals(mangaEntry, mockMangaEntry);
    }

    @Test
    public void testServiceFindByUserId() {
        List<MangaEntry> list = new ArrayList<>();
        list.add(mockMangaEntry);

        when(mangaEntryRepository.findAllByDiscordUser_Id(discordUser.getId()))
            .thenReturn(list);

        Iterable<MangaEntry> actual = mangaEntryService.getListMangaEntry(discordUser.getId());
        assertEquals(list, actual);
    }

    @Test
    public void testDelete() {
        mangaEntryService.deleteMangaEntry(mockMangaEntry);
    }

    @Test
    public void testUpdate() {
        mockMangaEntry.setRating(10);
        when(mangaEntryRepository.save(mockMangaEntry))
            .thenReturn(mockMangaEntry);
        MangaEntry actual = mangaEntryService.updateMangaEntry(mockMangaEntry);
        assertEquals(actual, mockMangaEntry);
    }

}