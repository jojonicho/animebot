package com.jojonicho.animebot.manga.commands.crud;

import static org.mockito.Mockito.when;

import com.jagrosh.jdautilities.command.CommandEvent;
import com.jojonicho.animebot.discorduser.model.DiscordUser;
import com.jojonicho.animebot.discorduser.repository.DiscordUserRepository;
import com.jojonicho.animebot.discorduser.service.DiscordUserServiceImpl;
import com.jojonicho.animebot.manga.commands.MangaEntryCommand;
import com.jojonicho.animebot.manga.model.MangaEntry;
import com.jojonicho.animebot.manga.service.MangaEntryService;
import net.dv8tion.jda.api.entities.User;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;

@ExtendWith(MockitoExtension.class)
public class UpdateMangaEntryEntryTest {

    @InjectMocks
    MangaEntryCommand mangaEntryCommand;

    @Mock
    CommandEvent event;

    private final String MANGA_ID = "11";
    private final String READ_ARGS = "list";

    private final String COMPLETED = "COMPLETED";
    private final String SCORE = "10";
    private final String UPDATE_ARGS = String.format("update %s %s %s",
            MANGA_ID, COMPLETED, SCORE);
    private final String UPDATE_ARGS_INVALID = "update " + MANGA_ID;

    private final String DISCORD_ID = "264258056416657419";

    @Mock
    private DiscordUserRepository discordUserRepository;

    @Mock
    private DiscordUserServiceImpl discordUserService;

    @Mock
    private MangaEntryService mangaEntryService;

    private DiscordUser discordUser;

    private MangaEntry mockMangaEntry;

    @Mock
    private User user;

    @BeforeEach
    public void setUp() {
        discordUser = new DiscordUser();
        discordUser.setId(DISCORD_ID);

        mockMangaEntry = new MangaEntry();
        mockMangaEntry.setDiscordUser(discordUser);
        mockMangaEntry.setMalId(Integer.parseInt(MANGA_ID));
    }

    @Test
    public void testExecuteUpdateNullMangaEntry() {
        when(event.getAuthor())
                .thenReturn(user);
        when(user.getId())
                .thenReturn(DISCORD_ID);
        when(event.getArgs())
                .thenReturn(UPDATE_ARGS);
        when(discordUserService.getDiscordUser(DISCORD_ID))
                .thenReturn(discordUser);
        mangaEntryCommand.execute(event);
    }

    @Test
    public void testExecuteUpdateFail() {
        when(event.getArgs())
                .thenReturn(UPDATE_ARGS_INVALID);
        mangaEntryCommand.execute(event);
    }

    @Test
    public void testExecuteUpdateSuccess() {
        when(event.getAuthor())
                .thenReturn(user);
        when(user.getId())
                .thenReturn(DISCORD_ID);
        when(event.getArgs())
                .thenReturn(UPDATE_ARGS);
        when(discordUserService.getDiscordUser(DISCORD_ID))
                .thenReturn(discordUser);
        when(mangaEntryService.getMangaEntry(mockMangaEntry.getMalId(), discordUser))
                .thenReturn(mockMangaEntry);
        mangaEntryCommand.execute(event);
    }

    @Test
    public void testExecuteUpdateThenRead() {
        when(event.getAuthor())
                .thenReturn(user);
        when(user.getId())
                .thenReturn(DISCORD_ID);
        when(event.getArgs())
                .thenReturn(UPDATE_ARGS);
        when(discordUserService.getDiscordUser(DISCORD_ID))
                .thenReturn(discordUser);
        when(mangaEntryService.getMangaEntry(mockMangaEntry.getMalId(), discordUser))
                .thenReturn(mockMangaEntry);
        mangaEntryCommand.execute(event);

        List<MangaEntry> list = new ArrayList<>();
        list.add(mockMangaEntry);

        when(event.getAuthor())
                .thenReturn(user);
        when(user.getId())
                .thenReturn(DISCORD_ID);
        when(event.getArgs())
                .thenReturn(READ_ARGS);
        when(mangaEntryService.getListMangaEntry(DISCORD_ID))
                .thenReturn(list);
        mangaEntryCommand.execute(event);
    }
}