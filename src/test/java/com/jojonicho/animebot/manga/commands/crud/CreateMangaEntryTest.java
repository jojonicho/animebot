package com.jojonicho.animebot.manga.commands.crud;

import static org.mockito.Mockito.when;

import com.jagrosh.jdautilities.command.CommandEvent;
import com.jojonicho.animebot.discorduser.model.DiscordUser;
import com.jojonicho.animebot.discorduser.service.DiscordUserServiceImpl;
import com.jojonicho.animebot.manga.commands.MangaEntryCommand;
import com.jojonicho.animebot.manga.model.MangaEntry;
import com.jojonicho.animebot.manga.service.MangaEntryService;
import net.dv8tion.jda.api.entities.User;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
public class CreateMangaEntryTest {

    @InjectMocks
    MangaEntryCommand mangaEntryCommand;

    @Mock
    CommandEvent event;

    private final String MANGA_ID = "11";
    private final String ADD_ARGS = "add " + MANGA_ID;

    private final String DISCORD_ID = "264258056416657419";

    @Mock
    private DiscordUserServiceImpl discordUserService;

    @Mock
    private MangaEntryService mangaEntryService;

    private DiscordUser discordUser;
    private MangaEntry mockMangaEntry1;

    @Mock
    private User user;

    @BeforeEach
    public void setUp() {
        discordUser = new DiscordUser();
        discordUser.setId(DISCORD_ID);

        mockMangaEntry1 = new MangaEntry();
        mockMangaEntry1.setDiscordUser(discordUser);
        mockMangaEntry1.setMalId(Integer.parseInt(MANGA_ID));
    }

    @Test
    public void testExecuteCreateMangaEntrySuccess() {
        when(event.getAuthor())
                .thenReturn(user);
        when(user.getId())
                .thenReturn(DISCORD_ID);
        when(event.getArgs())
                .thenReturn(ADD_ARGS);
        when(discordUserService.getDiscordUser(DISCORD_ID))
                .thenReturn(discordUser);
        when(mangaEntryService.createMangaEntry(mockMangaEntry1.getMalId(), discordUser))
                .thenReturn(mockMangaEntry1);
        mangaEntryCommand.execute(event);
    }

    @Test
    public void testExecuteCreateMangaEntryIncorrectArgument() {
        when(event.getArgs())
                .thenReturn("add naruto");
        mangaEntryCommand.execute(event);
    }

    @Test
    public void testExecuteCreateMangaEntryIncompleteArgument() {
        when(event.getArgs())
                .thenReturn("add");
        mangaEntryCommand.execute(event);
    }

    @Test
    public void testExecuteCreateAlreadyExistingMangaEntry() {
        when(event.getAuthor())
                .thenReturn(user);
        when(user.getId())
                .thenReturn(DISCORD_ID);
        when(event.getArgs())
                .thenReturn(ADD_ARGS);
        when(discordUserService.getDiscordUser(DISCORD_ID))
                .thenReturn(discordUser);
        when(mangaEntryService.createMangaEntry(mockMangaEntry1.getMalId(), discordUser))
                .thenReturn(mockMangaEntry1);
        mangaEntryCommand.execute(event);
    }

    @Test
    public void testExecuteCreateNonExistingJikanManga() {
        when(event.getAuthor())
                .thenReturn(user);
        when(user.getId())
                .thenReturn(DISCORD_ID);
        when(event.getArgs())
                .thenReturn("add 5");
        mangaEntryCommand.execute(event);
    }
}