package com.jojonicho.animebot.anime.service;

import static org.junit.jupiter.api.Assertions.assertEquals;

import com.jojonicho.animebot.anime.model.AnimeEntry;
import com.jojonicho.animebot.anime.model.JikanAnime;
import com.jojonicho.animebot.anime.model.JikanAnimeList;
import com.jojonicho.animebot.discorduser.model.DiscordUser;
import java.util.concurrent.CompletableFuture;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
public class JikanAnimeServiceImplTest {

    @InjectMocks
    private JikanAnimeServiceImpl jikanAnimeService;

    @Mock
    private CompletableFuture<JikanAnimeList> animeSearch;

    private DiscordUser discordUser;

    private AnimeEntry mockAnimeEntry;

    private static final String QUERY = "boku no hero academia";

    private static final String URL =
        "https://api.jikan.moe/v3/search/anime?q=boku no hero academia";

    private static final String[] QUERY_ARGS = new String[] {"boku", "no", "hero"};


    /**
     * setup.
     */
    @BeforeEach
    public void setUp() {
        discordUser = new DiscordUser();
        discordUser.setId("1");

        mockAnimeEntry = new AnimeEntry();
        mockAnimeEntry.setDiscordUser(discordUser);
        mockAnimeEntry.setMalId(1);
    }

    @Test
    public void testServiceCreate() {
        jikanAnimeService.getAnime(QUERY);
    }

    @Test
    public void testGetAnimeApi() {
        JikanAnime anime = jikanAnimeService.getAnime(QUERY);
        assertEquals(anime.getTitle().substring(0, QUERY.length()).toLowerCase(),
            QUERY);
    }

    @Test
    public void testGetAnimeApiQueryArgs() {
        JikanAnime anime = jikanAnimeService.getAnime(QUERY_ARGS);
        assertEquals(anime.getTitle().substring(0, QUERY.length()).toLowerCase(),
            QUERY);
    }
}
