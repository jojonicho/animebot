package com.jojonicho.animebot.anime.commands;

import static org.mockito.Mockito.when;

import com.jagrosh.jdautilities.command.CommandEvent;
import com.jojonicho.animebot.anime.model.AnimeEntry;
import com.jojonicho.animebot.anime.service.AnimeEntryService;
import com.jojonicho.animebot.discorduser.model.DiscordUser;
import java.util.ArrayList;
import java.util.List;
import net.dv8tion.jda.api.entities.User;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
public class AnimeListCommandTest {

    @InjectMocks
    private AnimeListCommand animeListCommand;

    @Mock
    private CommandEvent event;

    // demon slayer
    private static final String ANIME_ID = "40456";

    private static final String COMPLETED = "COMPLETED";
    private static final String SCORE = "10";

    private static final String DISCORD_ID = "264258056416657419";

    @Mock
    private AnimeEntryService animeEntryService;

    private DiscordUser discordUser;

    private AnimeEntry mockAnimeEntry;

    @Mock
    private User user;

    /**
     * setup.
     */
    @BeforeEach
    public void setUp() {
        discordUser = new DiscordUser();
        discordUser.setId(DISCORD_ID);

        mockAnimeEntry = new AnimeEntry();
        mockAnimeEntry.setDiscordUser(discordUser);
        mockAnimeEntry.setMalId(Integer.parseInt(ANIME_ID));
    }

    @Test
    public void testExecute() {
        List<AnimeEntry> list = new ArrayList<>();
        list.add(mockAnimeEntry);

        when(event.getAuthor())
            .thenReturn(user);
        when(user.getId())
            .thenReturn(DISCORD_ID);
        when(animeEntryService.getListAnimeEntry(DISCORD_ID))
            .thenReturn(list);
        animeListCommand.execute(event);
    }

    @Test
    public void testExecuteEmptyList() {
        List<AnimeEntry> list = new ArrayList<>();

        when(event.getAuthor())
            .thenReturn(user);
        when(user.getId())
            .thenReturn(DISCORD_ID);
        when(animeEntryService.getListAnimeEntry(DISCORD_ID))
            .thenReturn(list);
        animeListCommand.execute(event);
    }
}
