package com.jojonicho.animebot.anime.commands;

import static org.mockito.Mockito.when;

import com.jagrosh.jdautilities.command.CommandEvent;
import com.jojonicho.animebot.anime.model.JikanAnime;
import com.jojonicho.animebot.anime.service.JikanAnimeServiceImpl;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
public class AnimeSearchCommandTest {

    @InjectMocks
    private AnimeSearchCommand animeSearchCommand;

    @Mock
    private JikanAnimeServiceImpl jikanAnimeService;

    @Mock
    private CommandEvent event;

    private static final String QUERY = "boku no hero academia";
    private static final String[] QUERY_ARGS = new String[] {"boku", "no", "hero", "academia"};

    @Test
    public void testExecute() {
        JikanAnime anime = new JikanAnime()
            .setImageUrl(
                "https://cdn.myanimelist.net/images/anime/12/85221.jpg?s=bc4bd0a738a03bfe9176c5d83dc6f65e")
            .setMalId(1)
            .setTitle("")
            .setSynopsis("");

        when(jikanAnimeService.getAnime(QUERY_ARGS))
            .thenReturn(anime);
        when(event.getArgs())
            .thenReturn(QUERY);
        animeSearchCommand.execute(event);
    }

}
