package com.jojonicho.animebot.anime.commands;

import static org.mockito.Mockito.when;

import com.jagrosh.jdautilities.command.CommandEvent;
import com.jojonicho.animebot.anime.model.AnimeEntry;
import com.jojonicho.animebot.anime.service.AnimeEntryService;
import com.jojonicho.animebot.discorduser.model.DiscordUser;
import com.jojonicho.animebot.discorduser.repository.DiscordUserRepository;
import com.jojonicho.animebot.discorduser.service.DiscordUserServiceImpl;
import java.util.ArrayList;
import java.util.List;
import net.dv8tion.jda.api.entities.User;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
public class AnimeNextSeasonSearchCommandTest {

    @InjectMocks
    private AnimeNextSeasonSearchCommand animeNextSeasonSearchCommand;

    @Mock
    private CommandEvent event;

    private static final String ARGS = "5";
    // demon slayer
    private static final String ANIME_ID = "40456";

    private static final String COMPLETED = "COMPLETED";
    private static final String SCORE = "10";
    private static final String UPDATE_ARGS = String.format("update %s %s %s",
        ANIME_ID, COMPLETED, SCORE);
    private static final String UPDATE_ARGS_INVALID = "update " + ANIME_ID;

    private static final String DISCORD_ID = "264258056416657419";

    @Mock
    private DiscordUserRepository discordUserRepository;

    @Mock
    private DiscordUserServiceImpl discordUserService;

    @Mock
    private AnimeEntryService animeEntryService;

    private DiscordUser discordUser;

    private AnimeEntry mockAnimeEntry;

    @Mock
    private User user;

    /**
     * setup.
     */
    @BeforeEach
    public void setUp() {
        discordUser = new DiscordUser();
        discordUser.setId(DISCORD_ID);

        mockAnimeEntry = new AnimeEntry();
        mockAnimeEntry.setDiscordUser(discordUser);
        mockAnimeEntry.setMalId(Integer.parseInt(ANIME_ID));
    }

    @Test
    public void testExecuteEmptyArgs() {
        List<AnimeEntry> list = new ArrayList<>();
        list.add(mockAnimeEntry);

        when(event.getArgs())
            .thenReturn("");
        animeNextSeasonSearchCommand.execute(event);
    }

    @Test
    public void testExecute() {
        List<AnimeEntry> list = new ArrayList<>();
        list.add(mockAnimeEntry);

        when(event.getArgs())
            .thenReturn(ARGS);
        animeNextSeasonSearchCommand.execute(event);
    }
}
