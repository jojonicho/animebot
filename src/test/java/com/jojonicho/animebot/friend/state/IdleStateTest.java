package com.jojonicho.animebot.friend.state;

import com.jojonicho.animebot.discorduser.model.DiscordUser;
import com.jojonicho.animebot.friend.model.FriendRequest;
import com.jojonicho.animebot.friend.service.FriendRequestService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class IdleStateTest {
    @InjectMocks
    IdleState idleState;

    @Mock
    FriendRequestService friendRequestService;

    private FriendRequest friendRequest;
    private DiscordUser discordUser;
    private DiscordUser friendUser;

    @BeforeEach
    public void setUp() {
        discordUser = new DiscordUser();
        discordUser.setId("1223");
        friendUser = new DiscordUser();
        friendUser.setId("1224");

        friendRequest = new FriendRequest();
        friendRequest.setDiscordUser(discordUser);
        friendRequest.setFriendUser(friendUser);
        friendRequest.setUserName("dummy");
        friendRequest.setFriendName("dummy2");
        friendRequest.setState("IDLE");
    }

    @Test
    public void testAdd() {
        when(friendRequestService.getFriendRequest(discordUser, friendUser))
                .thenReturn(friendRequest);
        String result = idleState.addFriend(discordUser, friendUser, "dummy");
        assertEquals(result, "Succesfully added dummy to your friend request");
    }

    @Test
    public void testAcceptAndReject() {
        String result = idleState.acceptFriend(discordUser, friendUser, "dummy");
        assertEquals(result, "dummy never added you");

        result = idleState.rejectFriend(discordUser, friendUser, "dummy");
        assertEquals(result, "dummy never added you");
    }

    @Test
    public void testDelete() {
        String result = idleState.deleteFriend(discordUser, friendUser, "dummy");
        assertEquals(result, "dummy isn't your friend");
    }
}
