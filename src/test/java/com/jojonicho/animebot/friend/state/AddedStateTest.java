package com.jojonicho.animebot.friend.state;

import com.jojonicho.animebot.discorduser.model.DiscordUser;
import com.jojonicho.animebot.friend.model.FriendRequest;
import com.jojonicho.animebot.friend.repository.FriendRequestRepository;
import com.jojonicho.animebot.friend.service.FriendRequestService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class AddedStateTest {
    @InjectMocks
    AddedState addedState;

    @Mock
    FriendRequestRepository friendRequestRepository;

    @Mock
    FriendRequestService friendRequestService;

    private FriendRequest friendRequest;
    private DiscordUser discordUser;
    private DiscordUser friendUser;

    @BeforeEach
    public void setUp() {
        discordUser = new DiscordUser();
        discordUser.setId("1223");
        friendUser = new DiscordUser();
        friendUser.setId("1224");

        friendRequest = new FriendRequest();
        friendRequest.setDiscordUser(discordUser);
        friendRequest.setFriendUser(friendUser);
        friendRequest.setUserName("dummy");
        friendRequest.setFriendName("dummy2");
        friendRequest.setState("ADDED");
    }

    @Test
    public void testAdd() {
        when(friendRequestRepository.findByDiscordUserAndFriendUser(discordUser,friendUser))
                .thenReturn(friendRequest);
        String result = addedState.addFriend(discordUser, friendUser, "dummy");
        assertEquals(result, "You already added dummy please wait for his confirmation");

        when(friendRequestRepository.findByDiscordUserAndFriendUser(discordUser,friendUser))
                .thenReturn(null);
        result = addedState.addFriend(discordUser, friendUser, "dummy");
        assertEquals(result, "dummy already added you. Reply $friend <accept/reject> <name> to confirm.");
    }
    @Test
    public void testAccept() {
        when(friendRequestRepository.findByDiscordUserAndFriendUser(friendUser,discordUser))
                .thenReturn(friendRequest);
        String result = addedState.acceptFriend(discordUser, friendUser, "dummy");
        assertEquals(result, "Succesfully accepted dummy to your friend");

        when(friendRequestRepository.findByDiscordUserAndFriendUser(friendUser,discordUser))
                .thenReturn(null);
        result = addedState.acceptFriend(discordUser, friendUser, "dummy");
        assertEquals(result, "Invalid command, you must wait for dummy confirmation");
    }

    @Test
    public void testReject() {
        when(friendRequestRepository.findByDiscordUserAndFriendUser(friendUser,discordUser))
                .thenReturn(friendRequest);
        String result = addedState.rejectFriend(discordUser, friendUser, "dummy");
        assertEquals(result, "Succesfully rejected dummy");

        when(friendRequestRepository.findByDiscordUserAndFriendUser(friendUser,discordUser))
                .thenReturn(null);
        result = addedState.rejectFriend(discordUser, friendUser, "dummy");
        assertEquals(result, "Invalid command, you must wait for dummy confirmation");
    }

    @Test
    public void testDelete() {
        String result = addedState.deleteFriend(discordUser, friendUser, "dummy");
        assertEquals(result, "dummy isn't your friend");
    }
}
