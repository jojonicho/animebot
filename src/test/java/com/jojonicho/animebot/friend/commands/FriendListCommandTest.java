package com.jojonicho.animebot.friend.commands;

import com.jagrosh.jdautilities.command.CommandEvent;
import com.jojonicho.animebot.anime.model.AnimeEntry;
import com.jojonicho.animebot.anime.model.JikanAnime;
import com.jojonicho.animebot.anime.service.AnimeEntryService;
import com.jojonicho.animebot.anime.service.JikanAnimeService;
import com.jojonicho.animebot.discorduser.model.DiscordUser;
import com.jojonicho.animebot.discorduser.service.DiscordUserServiceImpl;
import com.jojonicho.animebot.friend.model.FriendRequest;
import com.jojonicho.animebot.friend.service.FriendRequestService;
import net.dv8tion.jda.api.entities.User;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.HashMap;
import java.util.Map;

import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class FriendListCommandTest {
    @InjectMocks
    FriendListCommand friendListCommand;

    @Mock
    CommandEvent event;

    @Mock
    private DiscordUserServiceImpl discordUserService;

    @Mock
    private FriendRequestService friendRequestService;

    @Mock
    private JikanAnimeService jikanAnimeServive;

    @Mock
    private AnimeEntryService animeEntryService;

    @Mock
    private User user;

    private DiscordUser discordUser;
    private DiscordUser friendUser;
    private FriendRequest mockFriendRequest;
    private final String DISCORD_ID = "264258056416657419";
    private final String FRIEND_ID = "708209585407655966";
    private final String ALL_ARGS = "all";
    private final String ANIME_ARGS = "anime witch hunter robin";
    private final String[] ANIME_INFO = new String[] {"witch", "hunter", "robin"};

    @BeforeEach
    public void setUp() {
        discordUser = new DiscordUser();
        discordUser.setId(DISCORD_ID);

        friendUser = new DiscordUser();
        friendUser.setId(FRIEND_ID);

        mockFriendRequest = new FriendRequest();
        mockFriendRequest.setDiscordUser(discordUser);
        mockFriendRequest.setFriendUser(friendUser);
        mockFriendRequest.setUserName("dummy");
        mockFriendRequest.setFriendName("dummy2");
        mockFriendRequest.setState("ACCEPTED");
    }

    @Test
    public void testFriendListAll() {
        Map<String, DiscordUser> friendList = new HashMap<String, DiscordUser>();
        friendList.put("dummy2", friendUser);
        when(event.getAuthor())
                .thenReturn(user);
        when(user.getId())
                .thenReturn(DISCORD_ID);
        when(event.getArgs())
                .thenReturn(ALL_ARGS);
        when(discordUserService.getDiscordUser(DISCORD_ID))
                .thenReturn(discordUser);
        when(friendRequestService.getAllFriend(discordUser))
                .thenReturn(friendList);
        friendListCommand.execute(event);
    }

    @Test
    public void testFriendListBasedOnAnime() {
        JikanAnime anime = new JikanAnime()
                .setImageUrl(
                        "https://cdn.myanimelist.net/images/anime/12/85221.jpg?s=bc4bd0a738a03bfe9176c5d83dc6f65e")
                .setMalId(1)
                .setTitle("")
                .setSynopsis("");

        AnimeEntry animeEntry = new AnimeEntry();
        animeEntry.setDiscordUser(friendUser);
        animeEntry.setMalId(1);

        Map<String, DiscordUser> friendList = new HashMap<String, DiscordUser>();
        friendList.put("dummy2", friendUser);
        when(event.getAuthor())
                .thenReturn(user);
        when(user.getId())
                .thenReturn(DISCORD_ID);
        when(event.getArgs())
                .thenReturn(ANIME_ARGS);
        when(discordUserService.getDiscordUser(DISCORD_ID))
                .thenReturn(discordUser);
        when(friendRequestService.getAllFriend(discordUser))
                .thenReturn(friendList);
        when(jikanAnimeServive.getAnime(ANIME_INFO))
                .thenReturn(anime);
        when(animeEntryService.getAnimeEntry(1, friendUser))
                .thenReturn(animeEntry);
        friendListCommand.execute(event);

        when(event.getAuthor())
                .thenReturn(user);
        when(user.getId())
                .thenReturn(DISCORD_ID);
        when(event.getArgs())
                .thenReturn(ANIME_ARGS);
        when(discordUserService.getDiscordUser(DISCORD_ID))
                .thenReturn(discordUser);
        when(friendRequestService.getAllFriend(discordUser))
                .thenReturn(friendList);
        when(jikanAnimeServive.getAnime(ANIME_INFO))
                .thenReturn(anime);
        when(animeEntryService.getAnimeEntry(1, friendUser))
                .thenReturn(null);
        friendListCommand.execute(event);
    }

    @Test
    public void testFriendListInvalidArgument() {
        when(event.getAuthor())
                .thenReturn(user);
        when(user.getId())
                .thenReturn(DISCORD_ID);
        when(event.getArgs())
                .thenReturn("sss");

        friendListCommand.execute(event);
    }
}
