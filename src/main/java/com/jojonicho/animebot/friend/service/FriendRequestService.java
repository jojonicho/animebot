package com.jojonicho.animebot.friend.service;

import com.jojonicho.animebot.discorduser.model.DiscordUser;
import com.jojonicho.animebot.friend.model.FriendRequest;

import java.util.Map;

public interface FriendRequestService {
    FriendRequest createFriendRequest(DiscordUser discordUser, DiscordUser friendUser,
                                      String userName, String friendName);

    Iterable<FriendRequest> getFriendRequestByFriend(DiscordUser friendUser);

    Iterable<FriendRequest> getFriendRequestByUser(DiscordUser discordUser);

    FriendRequest getFriendRequest(DiscordUser discordUser, DiscordUser friendUser);

    void updateFriendRequestState(String state, FriendRequest friendRequest);

    void deleteFriendRequest(FriendRequest friendRequest);

    Map<String, DiscordUser> getAllFriend(DiscordUser discordUser);
}
