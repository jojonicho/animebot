package com.jojonicho.animebot.friend.state;

import com.jojonicho.animebot.friend.model.FriendRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class StateHelper {
    @Autowired
    private IdleState idleFriendState;

    @Autowired
    private AddedState addedFriendState;

    @Autowired
    private AcceptedState acceptedFriendState;

    /**
     * Method that return state of friend request.
     */
    public State getRequestState(FriendRequest friendRequest) {
        try {
            String state = friendRequest.getState();
            State requestState;
            if (state.equals("ADDED")) {
                requestState = addedFriendState;
            } else if (state.equals("ACCEPTED")) {
                requestState = acceptedFriendState;
            } else {
                requestState = idleFriendState;
            }
            return requestState;
        } catch (NullPointerException e) {
            return idleFriendState;
        }
    }
}
