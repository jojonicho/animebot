package com.jojonicho.animebot.friend.state;

import com.jojonicho.animebot.discorduser.model.DiscordUser;
import com.jojonicho.animebot.friend.model.FriendRequest;
import com.jojonicho.animebot.friend.repository.FriendRequestRepository;
import com.jojonicho.animebot.friend.service.FriendRequestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class AddedState implements State {
    @Autowired
    private FriendRequestRepository friendRequestRepository;

    @Autowired
    private FriendRequestService friendRequestService;

    @Override
    public String addFriend(DiscordUser user, DiscordUser friend, String friendName) {
        FriendRequest req = friendRequestRepository.findByDiscordUserAndFriendUser(user, friend);
        if (req == null) {
            String message = " already added you. Reply $friend <accept/reject> <name> to confirm.";
            return friendName + message;
        }
        return String.format("You already added %s please wait for his confirmation", friendName);
    }

    @Override
    public String acceptFriend(DiscordUser user, DiscordUser friend, String friendName) {
        FriendRequest req = friendRequestRepository.findByDiscordUserAndFriendUser(friend, user);
        if (req == null) {
            return String.format("Invalid command, you must wait for %s confirmation", friendName);
        }
        friendRequestService.updateFriendRequestState("ACCEPTED", req);
        return String.format("Succesfully accepted %s to your friend", friendName);
    }

    @Override
    public String rejectFriend(DiscordUser user, DiscordUser friend, String friendName) {
        FriendRequest req = friendRequestRepository.findByDiscordUserAndFriendUser(friend, user);
        if (req == null) {
            return String.format("Invalid command, you must wait for %s confirmation", friendName);
        }
        friendRequestService.deleteFriendRequest(req);
        return String.format("Succesfully rejected %s", friendName);
    }

    @Override
    public String deleteFriend(DiscordUser user, DiscordUser friend, String friendName) {
        return friendName + " isn't your friend";
    }
}
