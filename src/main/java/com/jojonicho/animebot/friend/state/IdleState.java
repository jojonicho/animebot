package com.jojonicho.animebot.friend.state;

import com.jojonicho.animebot.discorduser.model.DiscordUser;
import com.jojonicho.animebot.friend.model.FriendRequest;
import com.jojonicho.animebot.friend.service.FriendRequestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class IdleState implements State {
    @Autowired
    private FriendRequestService friendRequestService;

    @Override
    public String addFriend(DiscordUser user, DiscordUser friend, String friendName) {
        FriendRequest friendRequest = friendRequestService.getFriendRequest(user, friend);
        friendRequestService.updateFriendRequestState("ADDED", friendRequest);
        return String.format("Succesfully added %s to your friend request", friendName);
    }

    @Override
    public String acceptFriend(DiscordUser user, DiscordUser friend, String friendName) {
        return friendName + " never added you";
    }

    @Override
    public String rejectFriend(DiscordUser user, DiscordUser friend, String friendName) {
        return friendName + " never added you";
    }

    @Override
    public String deleteFriend(DiscordUser user, DiscordUser friend, String friendName) {
        return friendName + " isn't your friend";
    }
}
