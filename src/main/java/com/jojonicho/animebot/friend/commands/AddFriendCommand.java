package com.jojonicho.animebot.friend.commands;

import com.jagrosh.jdautilities.command.Command;
import com.jagrosh.jdautilities.command.CommandEvent;
import com.jojonicho.animebot.discorduser.model.DiscordUser;
import com.jojonicho.animebot.discorduser.service.DiscordUserService;
import com.jojonicho.animebot.friend.model.FriendRequest;
import com.jojonicho.animebot.friend.service.FriendRequestService;
import com.jojonicho.animebot.friend.state.State;
import com.jojonicho.animebot.friend.state.StateHelper;
import java.time.format.DateTimeFormatter;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.Permission;
import net.dv8tion.jda.api.entities.Member;
import net.dv8tion.jda.api.entities.User;

public class AddFriendCommand extends Command {

    private StateHelper stateHelper;
    private final FriendRequestService friendRequestService;
    private final DiscordUserService discordUserService;

    /**
     * Command to add friend.
     */
    public AddFriendCommand(FriendRequestService friendRequestService,
                            DiscordUserService discordUserService, StateHelper stateHelper) {
        this.name = "add-friend";
        this.aliases = new String[]{"af"};
        this.help = "add friend to friend request";
        this.arguments = "<mention name>";
        this.botPermissions = new Permission[]{Permission.MESSAGE_EMBED_LINKS};
        this.guildOnly = false;
        this.friendRequestService = friendRequestService;
        this.discordUserService = discordUserService;
        this.stateHelper = stateHelper;
    }

    @Override
    protected void execute(CommandEvent event) {
        try {
            Member mention = event.getMessage().getMentionedMembers().get(0);
            User friend = mention.getUser();
            String friendId = friend.getId();
            DiscordUser friendUser = discordUserService.getDiscordUser(friendId);
            User user = event.getAuthor();
            String userId = user.getId();
            DiscordUser discordUser = discordUserService.getDiscordUser(userId);

            String description;
            if (!discordUser.equals(friendUser)) {
                String name = user.getAsMention();
                String friendName = friend.getAsMention();

                FriendRequest req = friendRequestService
                        .createFriendRequest(discordUser, friendUser, name, friendName);
                State requestState = stateHelper.getRequestState(req);
                description = requestState.addFriend(discordUser, friendUser, friendName);
                replyMessageHandler(true, description, event, mention);
            } else {
                description = "Oh, It's you. Please select other user.";
                replyMessageHandler(false, description, event, mention);
            }

        } catch (IndexOutOfBoundsException e) {
            String description = "You need to provide name as a mention!";
            replyMessageHandler(false, description, event, null);
        }

    }

    private void replyMessageHandler(boolean existUser, String description, CommandEvent event,
                                     Member friend) {
        if (existUser) {
            String joinedTime = friend.getTimeJoined().format(DateTimeFormatter.ISO_DATE);
            event.reply(new EmbedBuilder()
                    .setDescription(description)
                    .addField("Name: ", friend.getUser().getName(), false)
                    .addField("Member since: ", joinedTime, false)
                    .setThumbnail(friend.getUser().getEffectiveAvatarUrl())
                    .build());
        } else {
            event.reply(new EmbedBuilder()
                    .setDescription(description)
                    .build());
        }
    }
}
