package com.jojonicho.animebot.friend.commands;

import com.jagrosh.jdautilities.command.Command;
import com.jagrosh.jdautilities.command.CommandEvent;
import com.jojonicho.animebot.discorduser.model.DiscordUser;
import com.jojonicho.animebot.discorduser.service.DiscordUserService;
import com.jojonicho.animebot.friend.model.FriendRequest;
import com.jojonicho.animebot.friend.service.FriendRequestService;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.Permission;

public class AddFriendListCommand extends Command {
    private final FriendRequestService friendRequestService;
    private final DiscordUserService discordUserService;

    /**
     * Command to display friend that you added or odded you.
     */
    public AddFriendListCommand(FriendRequestService friendRequestService,
                                DiscordUserService discordUserService) {
        this.name = "add-friend-list";
        this.aliases = new String[]{"afl"};
        this.help = "get list of user that you added or added you";
        this.arguments = "<me/other>";
        this.botPermissions = new Permission[]{Permission.MESSAGE_EMBED_LINKS};
        this.guildOnly = false;
        this.friendRequestService = friendRequestService;
        this.discordUserService = discordUserService;
    }

    @Override
    protected void execute(CommandEvent event) {
        String[] args = event.getArgs().split("\\s+");
        String choice = args[0];

        String discordUserId = event.getAuthor().getId();
        DiscordUser discordUser = discordUserService.getDiscordUser(discordUserId);
        Iterable<FriendRequest> friendRequestList;

        if (choice.equalsIgnoreCase("me")) {
            friendRequestList = friendRequestService.getFriendRequestByUser(discordUser);
            displayUserList(event, friendRequestList, true);
        } else if (choice.equalsIgnoreCase("other")) {
            friendRequestList = friendRequestService.getFriendRequestByFriend(discordUser);
            displayUserList(event, friendRequestList, false);
        } else {
            event.reply("Invalid argument");
        }
    }

    private void displayUserList(CommandEvent event, Iterable<FriendRequest> requestList,
                                 boolean byUser) {
        String description = "";
        String title;
        if (byUser) {
            title = "List Of Users You Added";
            for (FriendRequest request : requestList) {
                description += request.getFriendName();
                if (request.getState().equals("ADDED")) {
                    description += " - " + "waiting for confirmation" + "\n";
                } else {
                    description += " - " + "Accepted" + "\n";
                }
            }
        } else {
            title = "List Of Users Who Added You";
            for (FriendRequest request : requestList) {
                description += request.getUserName();
                if (request.getState().equals("ADDED")) {
                    description += " - " + "waiting for confirmation" + "\n";
                } else {
                    description += " - " + "Accepted" + "\n";
                }
            }
        }
        event.reply(new EmbedBuilder()
                .setTitle(title)
                .setDescription(description)
                .build()
        );
    }
}
