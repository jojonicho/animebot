package com.jojonicho.animebot.anime.service;

import com.jojonicho.animebot.anime.model.JikanAnime;

public interface JikanAnimeService {

    JikanAnime getAnime(String[] queryArgs);

    JikanAnime getAnime(String query);
}
