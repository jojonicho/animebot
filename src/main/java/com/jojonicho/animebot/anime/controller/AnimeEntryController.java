package com.jojonicho.animebot.anime.controller;

import com.jojonicho.animebot.anime.model.AnimeEntry;
import com.jojonicho.animebot.anime.model.AnimeEntryRequest;
import com.jojonicho.animebot.anime.service.AnimeEntryService;
import com.jojonicho.animebot.discorduser.model.DiscordUser;
import com.jojonicho.animebot.discorduser.service.DiscordUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/anime-entry")
public class AnimeEntryController {

    @Autowired
    private AnimeEntryService animeEntryService;
    @Autowired
    private DiscordUserService discordUserService;

    /**
     * get list of anime entry.
     * @param discordId discord id.
     */
    @GetMapping(path = "/{discordId}", produces = {"application/json"})
    @ResponseBody
    public ResponseEntity getListAnimeEntry(@PathVariable(value = "discordId") String discordId) {
        Iterable<AnimeEntry> response = animeEntryService.getListAnimeEntry(discordId);
        if (null == response) {
            return constructNotFoundResponse();
        }
        return ResponseEntity.ok(response);
    }

    /**
     * create new anime entry.
     * @param discordId discord user id
     * @param req AnimeEntryRequest
     */
    @PostMapping(path = "/{discordId}", produces = {"application/json"})
    @ResponseBody
    public ResponseEntity createAnimeEntry(@PathVariable(value = "discordId") String discordId,
                                           @RequestBody AnimeEntryRequest req) {
        DiscordUser discordUser = discordUserService.getDiscordUser(discordId);
        AnimeEntry response = animeEntryService.createAnimeEntry(req.getMalId(), discordUser);
        if (null == response) {
            return constructNotFoundResponse();
        }
        return ResponseEntity.ok(response);
    }

    /**
     * updates anime entry.
     * @param discordId discordId.
     * @param req AnimeEntryRequest
     */
    @PutMapping (path = "/{discordId}", produces = {"application/json"})
    @ResponseBody
    public ResponseEntity updateAnimeEntry(@PathVariable(value = "discordId") String discordId,
                                           @RequestBody AnimeEntryRequest req) {
        DiscordUser discordUser = discordUserService.getDiscordUser(discordId);
        AnimeEntry animeEntry = animeEntryService.getAnimeEntry(req.getMalId(), discordUser);
        if (null == animeEntry) {
            return constructNotFoundResponse();
        }
        animeEntry.setStatus(req.getStatus());
        animeEntry.setRating(req.getRating());
        AnimeEntry response = animeEntryService.updateAnimeEntry(animeEntry);
        return ResponseEntity.ok(response);
    }

    /**
     * deletes anime entry.
     * @param discordId discord user id.
     * @param malId MyAnimeList id of anime.
     */
    @DeleteMapping (path = "/{discordId}/{malId}", produces = {"application/json"})
    @ResponseBody
    public ResponseEntity deleteAnimeEntry(@PathVariable(value = "discordId") String discordId,
                                           @PathVariable(value = "malId")  int malId) {
        DiscordUser discordUser = discordUserService.getDiscordUser(discordId);
        AnimeEntry animeEntry = animeEntryService.getAnimeEntry(malId, discordUser);
        animeEntryService.deleteAnimeEntry(animeEntry);
        return ResponseEntity.ok(malId);
    }


    private ResponseEntity constructNotFoundResponse() {
        return new ResponseEntity(HttpStatus.NOT_FOUND);
    }
}
