package com.jojonicho.animebot.anime.commands;

import com.github.doomsdayrs.jikan4java.core.Connector;
import com.github.doomsdayrs.jikan4java.data.enums.Season;
import com.github.doomsdayrs.jikan4java.data.model.main.season.SeasonSearch;
import com.github.doomsdayrs.jikan4java.data.model.main.season.SeasonSearchAnime;
import com.jagrosh.jdautilities.command.Command;
import com.jagrosh.jdautilities.command.CommandEvent;
import com.jagrosh.jdautilities.commons.waiter.EventWaiter;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.Permission;
import net.dv8tion.jda.api.entities.MessageEmbed;
import net.dv8tion.jda.api.events.message.react.MessageReactionAddEvent;

public class AnimeSeasonSearchCommand extends Command {

    private final EventWaiter waiter;
    private Long messageId;
    private List<SeasonSearchAnime> animes;
    private int page = 0;
    private static final String LEFT_EMOJI = "👈";
    private static final String RIGHT_EMOJI = "👉";
    public boolean isTest;
    public int stackDepth = 0;

    /**
     * Command to search anime from year and season.
     */
    public AnimeSeasonSearchCommand(EventWaiter waiter) {
        name = "season-search";
        aliases = new String[] {"ss"};
        help = "search anime from year and season";
        arguments = "<year> <summer/winter/spring/fall>";
        botPermissions = new Permission[] {Permission.MESSAGE_EMBED_LINKS};
        guildOnly = false;
        this.waiter = waiter;
    }

    @Override
    protected void execute(CommandEvent event) throws NumberFormatException {

        String[] args = event.getArgs().split(" ");
        int year = Integer.parseInt(args[0]);
        Season season = null;

        if (args[1].equalsIgnoreCase("summer")) {
            season = Season.SUMMER;
        } else if (args[1].equalsIgnoreCase("winter")) {
            season = Season.WINTER;
        } else if (args[1].equalsIgnoreCase("spring")) {
            season = Season.SPRING;
        } else if (args[1].equalsIgnoreCase("fall")) {
            season = Season.FALL;
        }
        if (null == season) {
            event.reactError();
            return;
        }

        CompletableFuture<SeasonSearch> search = new Connector().seasonSearch(year, season);
        SeasonSearch seasonSearch = search.join();
        setAnimes(seasonSearch.getAnimes());

        initializePagination(event);
        waiterWait(event);

    }

    /**
     * creates message embed.
     * @return MessageEmbed
     */
    public MessageEmbed getMessageEmbedByPage() {
        if (page >= animes.size()) {
            page = 0;
        } else if (page < 0) {
            page = animes.size() - 1;
        }

        SeasonSearchAnime anime = animes.get(page);
        String title = String.format("%d - %s", anime.getMalID(), anime.getTitle());
        return new EmbedBuilder()
            .setTitle(title)
            .setDescription(anime.getSynopsis())
            .setImage(anime.getImageURL())
            .build();
    }

    private void waiterWait(CommandEvent event) {
        if (isTest && stackDepth > 0) {
            return;
        }
        stackDepth++;
        waiter.waitForEvent(MessageReactionAddEvent.class,
            e -> e.getUserId().equals(event.getAuthor().getId())
                && e.getChannel().equals(event.getChannel()),
            e -> {
                String reactionCode = e.getReaction().getReactionEmote().getAsReactionCode();
                if (reactionCode.equals(RIGHT_EMOJI)) {
                    page++;
                }
                if (reactionCode.equals(LEFT_EMOJI)) {
                    page--;
                }

                MessageEmbed messageEmbed = getMessageEmbedByPage();
                event.getChannel()
                    .editMessageById(getMessageId(), messageEmbed)
                    .queue();
                waiterWait(event);
            }
        );
    }

    private void initializePagination(CommandEvent event) {
        MessageEmbed messageEmbed = getMessageEmbedByPage();
        event.reply(messageEmbed, (message -> {
            setMessageId(message.getIdLong());
            message.addReaction(LEFT_EMOJI).queue();
            message.addReaction(RIGHT_EMOJI).queue();
        }));
    }

    public Long getMessageId() {
        return messageId;
    }

    public void setMessageId(Long messageId) {
        this.messageId = messageId;
    }

    public void setPage(int page) {
        this.page = page;
    }

    public void setAnimes(List<SeasonSearchAnime> animes) {
        this.animes = animes;
    }

}
