package com.jojonicho.animebot.anime.commands;

import com.github.doomsdayrs.jikan4java.core.Connector;
import com.github.doomsdayrs.jikan4java.data.model.main.season.SeasonSearch;
import com.github.doomsdayrs.jikan4java.data.model.main.season.SeasonSearchAnime;
import com.jagrosh.jdautilities.command.Command;
import com.jagrosh.jdautilities.command.CommandEvent;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.Permission;

public class AnimeNextSeasonSearchCommand extends Command {
    private static final int NORMAL_LIMIT = 5;
    private static final int HARD_LIMIT = 20;

    /**
     * Command to show anime for the upcoming season.
     */
    public AnimeNextSeasonSearchCommand() {
        this.name = "season-search-next";
        this.aliases = new String[] {"ssn"};
        this.help = "shows anime for the upcoming season";
        this.arguments = "<number of animes>";
        this.botPermissions = new Permission[] {Permission.MESSAGE_EMBED_LINKS};
        this.guildOnly = false;
    }

    @Override
    protected void execute(CommandEvent event) {

        boolean hasArgs = !event.getArgs().isEmpty();
        String[] args = hasArgs ? event.getArgs().split("\\s+") : new String[] {};
        int numAnimes = NORMAL_LIMIT;

        if (hasArgs) {
            numAnimes = Math.min(HARD_LIMIT, Integer.parseInt(args[0]));
        }

        CompletableFuture<SeasonSearch> seasonSearchCompletableFuture =
            new Connector().seasonLater();
        SeasonSearch seasonSearch = seasonSearchCompletableFuture.join();
        List<SeasonSearchAnime> animes = seasonSearch.getAnimes().subList(0, numAnimes);

        for (SeasonSearchAnime anime : animes) {
            event.reply(new EmbedBuilder()
                .setDescription(anime.getSynopsis())
                .setImage(anime.getImageURL())
                .build());
        }

    }
}
