package com.jojonicho.animebot.manga.commands;

import com.jagrosh.jdautilities.command.Command;
import com.jagrosh.jdautilities.command.CommandEvent;
import com.jojonicho.animebot.manga.model.JikanManga;
import com.jojonicho.animebot.manga.service.JikanMangaService;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.Permission;

public class MangaSearchCommand extends Command {

    JikanMangaService jikanMangaService;

    /**
     *  Method for querying manga by title.
     * @param jikanMangaService Jikan Service
     */
    public MangaSearchCommand(JikanMangaService jikanMangaService) {
        this.name = "manga-search";
        this.aliases = new String[]{"ms"};
        this.help = "search for manga";
        this.arguments = "<query>";
        this.botPermissions = new Permission[]{Permission.MESSAGE_EMBED_LINKS};
        this.guildOnly = false;
        this.jikanMangaService = jikanMangaService;
    }

    @Override
    protected void execute(CommandEvent event) {
        String[] args  = event.getArgs().split("\\s+");

        JikanManga manga = jikanMangaService.getManga(args);

        if (null == manga) {
            String title = "Manga not found!";
            String description = String.format(
                    "It appears that we cannot find manga with the title: \"%s\" in our database",
                    event.getArgs()
            );

            event.reply(new EmbedBuilder()
                    .setTitle(title)
                    .setDescription(description)
                    .build());

            event.reactError();

            return;
        }

        String title = String.format("%d - %s", manga.getMalId(), manga.getTitle());
        event.reply(new EmbedBuilder()
                .setTitle(title)
                .setImage(manga.getImageUrl())
                .setDescription(manga.getSynopsis())
                .build());
    }
}
