package com.jojonicho.animebot.manga.commands.crud;

import com.github.doomsdayrs.jikan4java.data.model.main.manga.Manga;
import com.jagrosh.jdautilities.command.CommandEvent;
import com.jojonicho.animebot.discorduser.service.DiscordUserService;
import com.jojonicho.animebot.manga.service.MangaEntryService;
import net.dv8tion.jda.api.EmbedBuilder;

public abstract class CrudMangaEntry {
    public abstract void execute(CommandEvent event,
                                    MangaEntryService mangaEntryService,
                                    DiscordUserService discordUserService);

    /**
     * Method for when user does operations on
     * non-existing manga entry.
     * @param event CommandEvent
     * @param manga Manga
     */
    public void mangaEntryNotFoundErrorReply(CommandEvent event, Manga manga) {
        String title = String.format("You don't have %d - %s in your list!",
                manga.getMalID(), manga.getTitle());
        String description = String.format("type $manga add %d before update / delete",
                manga.getMalID());
        event.reply(new EmbedBuilder()
                .setTitle(title)
                .setDescription(description)
                .build());
        event.reactError();
    }

    /**
     * Method for when user arguments are incomplete.
     * @param event CommandEvents
     */
    public void insufficientArgumentErrorReply(CommandEvent event) {
        event.reply(new EmbedBuilder()
                .setTitle("Insufficient arguments!")
                .setDescription("type $help for the list of commands and their arguments")
                .build());
        event.reactError();
    }

    /**
     * Method for when user inputs an incorrect argument type.
     * @param event CommandEvents
     */
    public void incorrectArgumentErrorReply(CommandEvent event) {
        event.reply(new EmbedBuilder()
                .setTitle("Incorrect arguments!")
                .setDescription("You just inputted an incorrect type within the arguments. "
                       + "type $help for the list of commands and their arguments")
                .build());
        event.reactError();
    }
}
