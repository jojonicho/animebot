package com.jojonicho.animebot.manga.service;

import com.jojonicho.animebot.discorduser.model.DiscordUser;
import com.jojonicho.animebot.manga.model.MangaEntry;

public interface MangaEntryService {
    MangaEntry createMangaEntry(int malId, DiscordUser discordUser);

    MangaEntry getMangaEntry(int malId, DiscordUser discordUser);

    Iterable<MangaEntry> getListMangaEntry(String userId);

    MangaEntry updateMangaEntry(MangaEntry animeEntry);

    void deleteMangaEntry(MangaEntry animeEntry);
}
