package com.jojonicho.animebot.manga.service;

import com.jojonicho.animebot.manga.model.JikanManga;

public interface JikanMangaService {

    JikanManga getManga(String[] queryArgs);

    JikanManga getManga(String query);
}
