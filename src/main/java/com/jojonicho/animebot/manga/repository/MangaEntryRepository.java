package com.jojonicho.animebot.manga.repository;

import com.jojonicho.animebot.discorduser.model.DiscordUser;
import com.jojonicho.animebot.manga.model.MangaEntry;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MangaEntryRepository extends JpaRepository<MangaEntry, String> {
    MangaEntry findByMalIdAndDiscordUser(int malId, DiscordUser discordUser);

    List<MangaEntry> findAllByDiscordUser_Id(String userId);
}
