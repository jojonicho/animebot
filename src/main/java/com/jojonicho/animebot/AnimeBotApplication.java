package com.jojonicho.animebot;

import com.jagrosh.jdautilities.command.CommandClientBuilder;
import com.jagrosh.jdautilities.commons.waiter.EventWaiter;
import com.jagrosh.jdautilities.examples.command.AboutCommand;
import com.jagrosh.jdautilities.examples.command.ShutdownCommand;
import com.jojonicho.animebot.anime.commands.AnimeCommand;
import com.jojonicho.animebot.anime.commands.AnimeListCommand;
import com.jojonicho.animebot.anime.commands.AnimeNextSeasonSearchCommand;
import com.jojonicho.animebot.anime.commands.AnimeSearchCommand;
import com.jojonicho.animebot.anime.commands.AnimeSeasonSearchCommand;
import com.jojonicho.animebot.anime.service.AnimeEntryService;
import com.jojonicho.animebot.anime.service.JikanAnimeService;
import com.jojonicho.animebot.discorduser.service.DiscordUserService;
import com.jojonicho.animebot.friend.commands.AddFriendCommand;
import com.jojonicho.animebot.friend.commands.AddFriendListCommand;
import com.jojonicho.animebot.friend.commands.FriendConfirmationCommand;
import com.jojonicho.animebot.friend.commands.FriendListCommand;
import com.jojonicho.animebot.friend.service.FriendRequestService;
import com.jojonicho.animebot.friend.state.StateHelper;
import com.jojonicho.animebot.manga.commands.MangaEntryCommand;
import com.jojonicho.animebot.manga.commands.MangaSearchCommand;
import com.jojonicho.animebot.manga.service.JikanMangaService;
import com.jojonicho.animebot.manga.service.MangaEntryService;
import com.jojonicho.animebot.minigames.commands.RandomCommand;
import com.jojonicho.animebot.minigames.commands.SubstitutionCommand;
import com.jojonicho.animebot.minigames.commands.ThanosCommand;
import com.jojonicho.animebot.schedule.commands.Notify;
import com.jojonicho.animebot.schedule.commands.ReadFriendsList;
import com.jojonicho.animebot.schedule.commands.ScheduleCommand;
import com.jojonicho.animebot.schedule.commands.ScheduleListCommand;
import java.awt.Color;
import javax.annotation.PostConstruct;

import javax.security.auth.login.LoginException;

import javax.security.auth.login.LoginException;
import net.dv8tion.jda.api.JDABuilder;
import net.dv8tion.jda.api.OnlineStatus;
import net.dv8tion.jda.api.Permission;
import net.dv8tion.jda.api.entities.Activity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


@SpringBootApplication
public class AnimeBotApplication {

    private static final String PREFIX = "$";
    private static String TOKEN;
    private static String OWNER_ID;
    private final EventWaiter waiter;
    private final CommandClientBuilder client;
    @Autowired
    private AnimeEntryService animeEntryService;
    @Autowired
    private MangaEntryService mangaEntryService;
    @Autowired
    private DiscordUserService discordUserService;
    @Autowired
    private FriendRequestService friendRequestService;
    @Autowired
    private JikanAnimeService jikanAnimeService;
    @Autowired
    private JikanMangaService jikanMangaService;
    @Autowired
    private StateHelper stateHelper;

    /**
     * Main application for AnimeBot.
     */
    @Autowired
    public AnimeBotApplication(@Value("${discord_token}") String token,
                               @Value("${discord_owner_id}") String ownerId) {
        AnimeBotApplication.TOKEN = token;
        AnimeBotApplication.OWNER_ID = ownerId;
        waiter = new EventWaiter();
        client = new CommandClientBuilder();
    }

    public static void main(String[] args) throws IllegalArgumentException {
        SpringApplication app = new SpringApplication(AnimeBotApplication.class);
        app.run();
    }

    private void initClient() {
        // The default is "Type -help" (or whatver prefix you set)
        client.useDefaultGame();
        // sets the owner of the bot
        client.setOwnerId(AnimeBotApplication.OWNER_ID);
        // sets emojis used throughout the bot on successes, warnings, and failures
        client.setEmojis("U+1F4AF", "U+26A0", "U+1F47A");
        // sets the bot prefix
        client.setPrefix(AnimeBotApplication.PREFIX);
    }

    /**
     * Bot specific commands to run after Spring Boot.
     */
    @PostConstruct
    public void run() throws LoginException, IllegalArgumentException {
        initClient();

        client.addCommands(
            // command to show information about the bot
            new AboutCommand(Color.BLUE, "an com.jojonicho.animebot.example bot",
                new String[] {"Cool commands", "Nice examples", "Lots of fun!"},
                Permission.ADMINISTRATOR),

            // command to shut off the bot
            new ShutdownCommand()
        );

        addAnimeCommands();
        addMiniGamesCommands();
        addMangaCommands();
        addFriendCommands();
        addScheduleNotifierCommands();

        // start getting a bot account set up
        JDABuilder.createDefault(AnimeBotApplication.TOKEN)
            // set the game for when the bot is loading
            .setStatus(OnlineStatus.DO_NOT_DISTURB)
            .setActivity(Activity.playing("watching some anime..."))
            // add the listeners
            .addEventListeners(waiter, client.build())
            // start it up!
            .build();
    }

    private void addAnimeCommands() {
        // anime commands
        client.addCommands(
            new AnimeNextSeasonSearchCommand(),
            new AnimeSeasonSearchCommand(waiter),
            new AnimeListCommand(animeEntryService),
            new AnimeCommand(animeEntryService, discordUserService),
            new AnimeSearchCommand(jikanAnimeService)
        );
    }


    private void addMiniGamesCommands() {
        // anime commands
        client.addCommands(
            new RandomCommand(waiter, animeEntryService, discordUserService),
            new SubstitutionCommand(waiter, animeEntryService, discordUserService),
            new ThanosCommand(waiter, animeEntryService, discordUserService)

        );
    }


    private void addMangaCommands() {
        // anime commands
        client.addCommands(
            new MangaEntryCommand(mangaEntryService, discordUserService),
            new MangaSearchCommand(jikanMangaService)
        );
    }

    private void addScheduleNotifierCommands() {
        // schedule commands
        client.addCommands(
                //weekly list
                new ScheduleCommand(),
                new ScheduleListCommand(animeEntryService),
                new ReadFriendsList(friendRequestService, discordUserService, animeEntryService),
                new Notify(animeEntryService)
        );
    }

    private void addFriendCommands() {
        // friend commands
        client.addCommands(
                new AddFriendCommand(friendRequestService, discordUserService, stateHelper),
                new AddFriendListCommand(friendRequestService, discordUserService),
                new FriendListCommand(friendRequestService, discordUserService,
                        jikanAnimeService, animeEntryService),
                new FriendConfirmationCommand(friendRequestService, discordUserService, stateHelper)
        );
    }
}
