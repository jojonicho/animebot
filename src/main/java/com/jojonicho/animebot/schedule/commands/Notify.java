package com.jojonicho.animebot.schedule.commands;

import com.github.doomsdayrs.jikan4java.core.Connector;
import com.github.doomsdayrs.jikan4java.data.model.main.anime.Anime;
import com.github.doomsdayrs.jikan4java.data.model.main.schedule.Schedule;
import com.github.doomsdayrs.jikan4java.data.model.main.schedule.SubAnime;
import com.jagrosh.jdautilities.command.Command;
import com.jagrosh.jdautilities.command.CommandEvent;
import com.jojonicho.animebot.anime.model.AnimeEntry;
import com.jojonicho.animebot.anime.service.AnimeEntryService;
import java.time.DayOfWeek;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Objects;
import java.util.concurrent.CompletableFuture;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.Permission;


public class Notify extends Command {

    private final AnimeEntryService animeEntryService;

    /**
     * Method untuk menampilkan jadwal anime berdasarkan list user.
     */
    public Notify(AnimeEntryService animeEntryService) {
        this.name = "notify-user";
        this.aliases = new String[]{"n"};
        this.help = "get notifications of the latest anime episodes based on the user list";
        this.botPermissions = new Permission[]{Permission.MESSAGE_EMBED_LINKS};
        this.guildOnly = false;
        this.animeEntryService = animeEntryService;
    }

    @Override
    protected void execute(CommandEvent event) {

        String discordUserId = event.getAuthor().getId();

        Iterable<AnimeEntry> animeEntryList = animeEntryService.getListAnimeEntry(discordUserId);

        if (!animeEntryList.iterator().hasNext()) {
            event.reactError();
            return;
        }

        CompletableFuture<Schedule> scheduleFuture = new Connector().getCurrentSchedule();
        Schedule schedule = scheduleFuture.join();
        ArrayList<ArrayList<SubAnime>> sc = new ArrayList<>();
        String[] day = {"MONDAY","TUESDAY","WEDNESDAY","THURSDAY","FRIDAY","SATURDAY","SUNDAY"};
        sc.add(schedule.getMonday());
        sc.add(schedule.getTuesday());
        sc.add(schedule.getWednesday());
        sc.add(schedule.getThursday());
        sc.add(schedule.getFriday());
        sc.add(schedule.getSaturday());
        sc.add(schedule.getSunday());

        String avatarImageUrl = event.getAuthor().getAvatarUrl();
        StringBuilder description = new StringBuilder();

        int count = 0;

        DayOfWeek currentDay = LocalDate.now().getDayOfWeek();
        for (AnimeEntry animeEntry: animeEntryList) {
            int malId = animeEntry.getMalId();
            String eps = null;

            for (ArrayList<SubAnime> listAnime : sc) {
                count += 1;
                if (count > 6) {
                    count = 0;
                }
                for (SubAnime anime : listAnime) {
                    if (malId == anime.getMalID()) {
                        eps = String.valueOf((anime.getEpisodeCount()));
                        count--;
                        if (count == -1) {
                            count = 6;
                        }
                    }
                }
            }

            if (Objects.equals(eps, "0")) {
                eps = "can't be found in database";
            }

            Anime idAnime = new Connector().retrieveAnime(malId).join();

            description.append(String.format("Episode %s - %d - %s\n",
                    eps, malId, idAnime.getTitle()));
        }
        String title = String.format("%s update", currentDay);

        event.reply(new EmbedBuilder()
                .setTitle(title)
                .setDescription(description.toString())
                .setThumbnail(avatarImageUrl)
                .build()
        );
    }
}
