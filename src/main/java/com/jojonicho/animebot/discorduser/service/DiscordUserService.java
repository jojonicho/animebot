package com.jojonicho.animebot.discorduser.service;

import com.jojonicho.animebot.discorduser.model.DiscordUser;

public interface DiscordUserService {

    DiscordUser createDiscordUser(String id);

    DiscordUser getDiscordUser(String id);
}
