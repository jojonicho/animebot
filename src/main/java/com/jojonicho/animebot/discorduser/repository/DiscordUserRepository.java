package com.jojonicho.animebot.discorduser.repository;

import com.jojonicho.animebot.discorduser.model.DiscordUser;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface DiscordUserRepository extends JpaRepository<DiscordUser, String> {
    DiscordUser getDiscordUserById(String id);
}
